#include "serialize.h"
#include <unordered_map>
#include "tcp.h"
#include <string>
#include "user.h"
#include <iostream>

#ifndef __REQUEST_REPLY__H_
#define __REQUEST_REPLY__H_

extern tcp ztcp;
// extern tcp ytcp;

using std::cout;
using std::endl;

extern std::string _dir_name;
extern std::string _my_name;
extern std::string _sender_name;
extern int __rquota_i;
std::string dir_name();
std::string my_name();
std::string sender_name();

data request_helper(host h, data d, tcp& );

data listen(host h, tcp& );

bool reply(host h, data d, tcp& );

char* array_byting(std::vector<uint8_t> st);

StreamType byte_streaming(char* begin,char* end);

// template<typename... T>
// data rpc(int f, data d,std::tuple<T...> argT){
//   d.img = new char[50];
//   d.function = f;
//   StreamType st;
//   serialize(argT,st);
//   d.img = array_byting(st);
//   d.size = st.size();
//
//   //udp.ibind(host());
//   data ud = request_helper(host("0.0.0.0", 1027), d, xtcp);
//   return ud;
// }

void f();

int g(int x);

int inc_quota(int k);



extern std::unordered_map<std::string, int> name_to_fun_num;

template<typename...T,typename...A >
std::tuple<T...> request(std::string user,std::string req,std::tuple<A...> args)
{
  data d;
  d.owner = new char[10];
  d.receiver = new char[10];
  memcpy(d.receiver, user.c_str(), 10);
  memcpy(d.owner, my_name().c_str(), 10); 
  StreamType result;
  serialize(args,result);
  d.img = array_byting(result);
  d.size = result.size();
  d.function = name_to_fun_num[req];
  d.quota = __rquota_i;
  int p;
  if(user == "directory"){
    p = 1030;
  }
  else{
    p = 1040;
  }
  tcp t{true};
  t.udp.ibind(host{});
  std::cout << user << " "<< get<0>(users[user]) << " " << p << std::endl;
  data res = request_helper(host(get<0>(users[user]), p), d, t);
  auto ret = deserialize<std::tuple<T...>>(byte_streaming(res.img,res.img+res.size));
  return ret;
}





#endif //__REQUEST_REPLY__H_
