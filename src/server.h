#include "message.h"
#include "host.h"
#include <utility>

#ifndef __SERVER__H_
#define __SERVER__H_

//TODO: inheret from net_node
/*!Abstract server*/
class server{
public:
  virtual void exec() = 0;
  virtual std::pair<message*, host> receive() = 0;
  virtual bool reply(message*, host) = 0;
  virtual ~server(){};
};

#endif //__SERVER__H_
