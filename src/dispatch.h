#include "request_reply.h"
#include "service.h"
#include "Steg.h"
#ifndef __DISPATCH__H_
#define __DISPATCH__H_


template<typename... A>
void dispatch(data ud){
  data d;
  d.owner = new char[10];
  memcpy(d.owner, my_name().c_str(), 10);
  d.receiver = new char[10];
  strcpy(d.receiver, ud.owner);
  StreamType argStream;
  // tcp ztcp{true};
  _sender_name = string{ud.receiver};
  switch (ud.function) {
    case 0:
      {
        std::cout << "dispatch: received img" << std::endl;
        argStream = byte_streaming(ud.img,ud.img+ud.size);
        auto args0 = deserialize<std::tuple<std::string>>(argStream);
        vector<uint8_t> imgr;
        string img_name = get<0>(args0).substr(1, get<0>(args0).length());
        cout << "kossom el img: " << img_name << endl;

        Steg senz('s',img_name);
        senz.steg_ds();
        string qfile = senz.data_path;
        ofstream test(qfile);

        test <<my_name();   //GET USERNAME function
        test <<endl;
        test <<ud.owner;
        test <<endl;
        test <<ud.quota;
        test.close();
        cout << ud.owner << " 3nd refaat" << endl;
        int x;
        // sen.steg_ds();
        cin>> x;

        // cout <<"EL ESM EL BB3to" <<get<0>(args0) <<endl;
        imgr = send_img(get<0>(args0));
        // imgr = send_img(img_name);

        std::cout << " Img send is executed" << endl;
        tuple<vector<uint8_t>> t1{imgr};
        StreamType s;
        serialize(t1,s);
        d.size = s.size();
        d.img = new char[d.size];
        cout << "qouta elly estlmtha: " << ud.quota << endl;
        memcpy(d.img, array_byting(s), s.size());
        if(reply(ztcp.connected_to_me,d, ztcp))
          cout << "Image is successfully sent :) " << endl;
        break;
    }
    case 1:
    {
      std::cout << "dispatch: calling Increase Quota function" << std::endl;
      argStream = byte_streaming(ud.img,ud.img+ud.size);
      auto args1 = deserialize<std::tuple<int,char>>(argStream);
      std::cout << " RPC is Done !!"<< inc_quota(std::get<0>(args1)) << std::endl;
      if(reply(ztcp.connected_to_me,d, ztcp))
        std::cout << "Tmam Kda !" << std::endl;
      break;
    }
    case 2:
    {
      std::cout << "dispatch: dir" << std::endl;
      argStream = byte_streaming(ud.img,ud.img+ud.size);
      //auto args2 = deserialize<std::tuple<int>>(argStream);
      vector<tuple<string,user_info>> dirf;
      std::cout << " Dir is executed" << endl;
      dirf = dir();
      tuple<vector<tuple<string,user_info>>> t1{dirf};
      StreamType result;
      serialize(t1,result);
      d.size = result.size();
      d.img = new char[d.size];
      memcpy(d.img,array_byting(result),result.size());
      if(reply(ztcp.connected_to_me,d, ztcp))
        cout << "Directory is updated :) " << endl;
      break;
    }
    case 3:
    {
      std::cout << "dispatch: verify user" << std::endl;
      argStream = byte_streaming(ud.img,ud.img+ud.size);
      auto args2 = deserialize<std::tuple<string,string>>(argStream);
      bool verf;
      verf = verify_user(get<0>(args2), get<1>(args2));
      tuple<bool> t1{verf};
      StreamType result;
      serialize(t1,result);
      d.size = result.size();
      d.img = new char[d.size];
      memcpy(d.img,array_byting(result),result.size());
      if(reply(ztcp.connected_to_me,d, ztcp))
        cout << "User is verified" << endl;
      break;
    }
    case 7:
    {
        std::cout << "dispatch: Register User" << std::endl;
        argStream = byte_streaming(ud.img,ud.img+ud.size);
        auto args2 = deserialize<std::tuple<string,string>>(argStream);
        bool verf;
        std::cout << " Dir is executed" << endl;
        verf = register_user(get<0>(args2),get<1>(args2));
        tuple<bool>t1{verf};
        StreamType result;
        serialize(t1,result);
        d.size = result.size();
        d.img = new char[d.size];
        memcpy(d.img,array_byting(result),result.size());
        if(reply(ztcp.connected_to_me,d, ztcp))
          cout << "Register is Done :) " << endl;
        break;
    }
    case 6:
    {
        std::cout << "dispatch: Get Image Names" << std::endl;
        argStream = byte_streaming(ud.img,ud.img+ud.size);
        auto args2 = deserialize<std::tuple<string>>(argStream);
        vector<string> names;
        std::cout << " get_img_names is executed" << endl;
        names = img_names();
        tuple<vector<string>>t1{names};
        StreamType result;
        serialize(t1,result);
        d.size = result.size();
        d.img = new char[d.size];
        memcpy(d.img,array_byting(result),result.size());
        if(reply(ztcp.connected_to_me,d, ztcp))
          cout << "Image names are viewed :) " << endl;
        break;
    }
    case 8:
    {
      std::cout << "dispatch: Recieve Image " << std::endl;
      argStream = byte_streaming(ud.img,ud.img+ud.size);
      auto args = deserialize<std::tuple<vector<uint8_t>,string>>(argStream);
      bool verf;
      verf = recieve_img(get<0>(args), get<1>(args));
      tuple<bool> t1{verf};
      StreamType result;
      serialize(t1,result);
      d.size = result.size();
      d.img = new char[d.size];
      memcpy(d.img,array_byting(result),result.size());
      if(reply(ztcp.connected_to_me,d, ztcp))
        cout << "Image is Recieved and Saved as " << get<1>(args) << endl;
    }

      // case 5:
    //   {
    //     break;
    //   }
    //   case 6:
    //   {
    //     break;
    //   }
    default:
      std::cout << "dispatch: undefined function" << std::endl;
  }

}

#endif //__DISPATCH__H_
