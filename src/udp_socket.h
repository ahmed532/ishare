#include "isocket.h"
#include "host.h"
#include "message.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <utility>
#include <iostream>
#include <string.h>

#ifndef __UDP_SOCKET__H_
#define __UDP_SOCKET__H_

/*!Implements a UDP socket*/
class udp_socket : public isocket {
public:
  udp_socket(bool);
  bool ibind(host);
  int isendto(message*, host);
  std::pair<host, int> irecvfrom(message*); // TODO: should create a message
                                            //       not take a parameter
                                            //       then return the message
                                            //       and host in pair
  ~udp_socket();
private:
  host socket_host;
  int s;
};

void setBroadcast(int s);

#endif //__UDP_SOCKET__H_
