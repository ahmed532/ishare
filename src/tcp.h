#include <bits/stdc++.h>
#include "host.h"
#include "udp_socket.h"
#include "char_message.h"
#include <stdexcept>

#ifndef __TCP__H_
#define __TCP__H_

static const unsigned int max_packet_size = 65507;
static const unsigned int max_number_packets = 10000;
static const unsigned int max_payload_size = 65499;
static const unsigned int max_byte_size = 100000000;

class timeout_error : public std::runtime_error{
public:
  timeout_error() :std::runtime_error("tcp timeout error"){}
};

struct data{
  int quota;
  int size;
  int function;
  char* img;
  char* owner;
  char* receiver;
};

struct packet{
  int seq;
  int ack;
  unsigned char payload[max_payload_size];
};

class tcp{
public:
  tcp(bool);
  int tcp_counter;
  int tcp_packets_size;
  udp_socket udp;
  packet* tcp_packets;
  packet* tcp_rec_packets;

  int tcp_receive_count;
  host connected_to_me;
  int ack_rec,seq_rec;

  char* marshal(data d);
  void tcp_shred(char* m, int n);
  char* extract_payload();
  char* marshal_packet(packet s);
  bool tcp_send_next(host h);
  int tcp_receive_next();
  void tcp_send(char* m, unsigned int n, host h);
  char* tcp_receive ();
  data unmarshal (char* p);
};

#endif //__TCP__H_
