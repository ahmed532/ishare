#include "tcp.h"

using std::cout;
using std::endl;

tcp::tcp(bool timeout)
:udp(timeout), connected_to_me(), ack_rec(0), seq_rec(0){
  tcp_packets = new packet[max_number_packets];
  tcp_rec_packets = new packet[max_number_packets];
}

char* tcp::marshal(data d){
  char* a = new char[max_byte_size];
  memcpy(a, &d, sizeof d);
  memcpy((a+sizeof d), d.owner, 10); //TODO: usename size
  memcpy((a+sizeof d + 10), d.receiver, 10);
  memcpy((a+sizeof d + 10 + 10), d.img, d.size);
  return a;
}

void tcp::tcp_shred(char* m, int n){
  unsigned int i;
  memset(tcp_packets, 0, max_number_packets);
  for(i = 0; i <= n/max_payload_size; ++i){
    memcpy(tcp_packets[i].payload, m+i*max_payload_size, max_payload_size);
    tcp_packets[i].seq = i;
    tcp_packets[i].ack = 0;
  }
  //tcp_packets[i-1].seq = 10001;
  tcp_packets[i-1].ack = 10001;

  tcp_counter = 0;
  tcp_packets_size = i;
}

char* tcp::extract_payload() {
  char* m = new char[max_byte_size];
  for(int i = 0; i < tcp_receive_count;i++) {
    memcpy(m + i*max_payload_size, tcp_rec_packets[i].payload, max_payload_size);
  }
  return m;
}

char* tcp::marshal_packet(packet s) {
  char* m = new char[max_packet_size];
  memcpy(m, &s.seq, sizeof s.seq);
  memcpy(m+sizeof s.seq,&s.ack, sizeof s.ack);
  memcpy(m+sizeof s.seq+sizeof s.ack, s.payload, max_payload_size);
  return m;
}

bool tcp::tcp_send_next(host h) {
  if(tcp_counter == tcp_packets_size) return false;
  char* k = marshal_packet(tcp_packets[tcp_counter]);
  char_message m(k, k + max_packet_size);
  udp.isendto(&m, h);
  return true;
}

void tcp::tcp_send(char* m, unsigned int n, host h) {
  tcp_shred(m, n);
  int ack = 0;
  int timeout_trials = 5;
  while (tcp_send_next(h)) {
    char_message m(max_packet_size);
    if(timeout_trials > 0){
      auto p = udp.irecvfrom(&m);
      if(p.second > 0){
        timeout_trials = 5;
      }
      else{
        --timeout_trials;
        continue;
      }
    }
    else{
      throw timeout_error{};
    }

    const char* mc = m.marshal();
    ack = int((unsigned char)(mc[7]) << 24 |
               (unsigned char)(mc[6]) << 16 |
               (unsigned char)(mc[5]) << 8 |
               (unsigned char)(mc[4]));
    tcp_counter = ack;
  }
}

int tcp::tcp_receive_next() {
  char_message m(max_packet_size);
  auto p = udp.irecvfrom(&m);
  if(p.second <= 0){
    return -1;
  }
  connected_to_me = p.first;
  const char* mc = m.marshal();
  int seq = int((unsigned char)(mc[3]) << 24 |
             (unsigned char)(mc[2]) << 16 |
             (unsigned char)(mc[1]) << 8 |
             (unsigned char)(mc[0]));
  int ack = int((unsigned char)(mc[7]) << 24 |
            (unsigned char)(mc[6]) << 16 |
            (unsigned char)(mc[5]) << 8 |
            (unsigned char)(mc[4]));
  tcp_receive_count = seq+1;
  memcpy(tcp_rec_packets[seq].payload, mc+8, max_payload_size);
  tcp_rec_packets[seq].seq = seq;
  tcp_rec_packets[seq].ack = ack;
  ack_rec = ack;
  seq_rec = seq+1;
  return p.second;
}

char* tcp::tcp_receive () {
  memset(tcp_rec_packets, 0, max_number_packets);
  ack_rec = 0;
  seq_rec = 0;
  int timeout_trials = 5;
  while(ack_rec != 10001){
    if(timeout_trials > 0) {
      int x = tcp_receive_next();
      if(x > 0){
        packet echo_packet;
        echo_packet.seq = 0;
        echo_packet.ack = seq_rec;
        char* rec = marshal_packet(echo_packet);
        char_message msg(rec,rec+max_packet_size);
        timeout_trials = 5;
        udp.isendto(&msg,connected_to_me);
      }
      else{
        if(seq_rec == 0){
          timeout_trials = 5;
          continue;
        }
        else{
          packet echo_packet;
          echo_packet.seq = 0;
          echo_packet.ack = seq_rec;
          char* rec = marshal_packet(echo_packet);
          char_message msg(rec,rec+max_packet_size);
          udp.isendto(&msg,connected_to_me);
          --timeout_trials;
        }
      }
    }
    else{
      throw timeout_error{};
    }
  }
  return extract_payload();
}

data tcp::unmarshal (char* p) {
  data d;
  d.quota = int((unsigned char)(p[3]) << 24 |
             (unsigned char)(p[2]) << 16 |
             (unsigned char)(p[1]) << 8 |
             (unsigned char)(p[0]));
  d.size = int((unsigned char)(p[7]) << 24 |
             (unsigned char)(p[6]) << 16 |
             (unsigned char)(p[5]) << 8 |
             (unsigned char)(p[4]));
  d.function = int((unsigned char)(p[11]) << 24 |
             (unsigned char)(p[10]) << 16 |
             (unsigned char)(p[9]) << 8 |
             (unsigned char)(p[8]));
  d.owner = new char[10];
  d.receiver = new char[10];
  d.img = new char[d.size];
  memcpy(d.owner, p + sizeof d, 10);
  memcpy(d.receiver, p + sizeof d + 10, 10);
  memcpy(d.img, p + sizeof d + 10 + 10, d.size);
  return d;
}
