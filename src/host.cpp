#include "host.h"

/*!Claim a port in a given host*/
host::host(std::string hostname, int port)
:_port(port),_hostname(hostname) {
  // struct sockaddr_in _address; This is a local variable FUCK!
  if(_hostname==""){
    _address.sin_family = AF_INET;
    _address.sin_port = htons(port);
    _address.sin_addr.s_addr = htonl(INADDR_ANY);
  }
  else{
    struct hostent *h;
    _address.sin_family = AF_INET;
    if((h = gethostbyname(hostname.c_str()))== NULL){
      printf("Unknown host name\n");
      exit(-1);
    }
    _address.sin_addr = *(struct in_addr *) (h->h_addr);
    _address.sin_port = htons(port);
  }
}

int host::port(){
  return ntohs(_address.sin_port);;
}

/*!Pretty print "protocol:ip:port"*/
void host::print_info() {
  printf("\x1b[34m%d\x1b[0m:\x1b[34m%s\x1b[0m:\x1b[34m%d\x1b[0m", _address.sin_family,
    inet_ntoa(_address.sin_addr), ntohs(_address.sin_port));
}

struct sockaddr * host::address() {
  return (struct sockaddr *) &_address;
}

struct sockaddr_in* host::address_in() {
  return &_address;
}

int host::address_size() {
  return sizeof(struct sockaddr_in);
}

std::string host::hostname() {
  return std::string{inet_ntoa(_address.sin_addr)};
}
