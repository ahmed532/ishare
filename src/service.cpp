#include "service.h"

bool register_user(string user_name, string ip){
	ofstream dir_out("../static/directory.txt", std::ios_base::app);
	dir_out << user_name << " " << ip << endl;
	users[user_name] = user_info{ip, 1};
	return true;
}

bool verify_user(string user_name, string ip){
	if(users.find(user_name) == users.end()){
		return false;
	}
	else{
		users[user_name] = user_info{ip, 1};
		return true;
	}
}

bool login(string user_name, string ip){
	return get<0>(request<bool>(dir_name(), "login", tuple<string, string>{user_name, ip}));
}

bool sign_up(string user_name, string ip){
	return get<0>(request<bool>(dir_name(), "sign_up", tuple<string, string>{user_name, ip}));
}

void read_dir(){
	std::ifstream fin{"../static/directory.txt"};
	string user, ip;
	while(fin>>user>>ip){
		if(users.find(user) == users.end())
			users[user] = user_info{ip, 0}; //TODO: find out default online or offline
	}
}

vector<tuple<string, user_info>> dir(){
	read_dir();
	vector<tuple<string, user_info>> v{};
	copy(users.begin(), users.end(), std::back_inserter(v));
	return v;
}

vector<tuple<string, user_info>> get_dir(){
	return get<0>(request<vector<tuple<string, user_info>>>(dir_name(),
	 "get_dir", tuple<int>{5}));
}

vector<uint8_t> get_img(string user_name, string img_name){
	return get<0>(request<vector<uint8_t>>(user_name, "get_img", tuple<string>{img_name}));
}

vector<uint8_t> send_img(string img_name){
	std::ifstream fin{img_name};
	// std:string temp = "../static/"+my_name()+"_mimgs/" + img_name;
	// cout << "TEMP SEND IMG" <<temp <<endl;
	cout << img_name << "dsjdlskfj ******" << endl;
	FILE* img_in;
	if(img_name.at(0)=='x')
	img_in = fopen(std::string{"../static/" + img_name.substr(1, img_name.length())}.c_str(), "rb");
	else
	img_in = fopen(std::string{"../static/" + img_name}.c_str(), "rb");

	// FILE* img_in = fopen(temp.c_str(), "rb");

	fseek(img_in, 0, SEEK_END);          // Jump to the end of the file
  int filelen = ftell(img_in);             // Get the current byte offset in the file
  rewind(img_in);                      // Jump back to the beginning of the file

  char* buffer = (char *)malloc((filelen+1)*sizeof(char)); // Enough memory for file + \0
  fread(buffer, filelen, 1, img_in); // Read in the entire file
  fclose(img_in); // Close the file
	vector<uint8_t> v(buffer, buffer+filelen);
	return v;
}

bool send_to_user(string user_name,string img_name)
{
	auto v = send_img(img_name);
	return get<0>(request<bool>(user_name,"recieve_img",tuple<vector<uint8_t>,string>{v,img_name}));
}

bool recieve_img(vector<uint8_t> img_stream,string img_name)
{
	bool flag = false;
	char* img = array_byting(img_stream);
	FILE* img_in = fopen(std::string{"../static/"+my_name()+"_rimgs/" + img_name}.c_str(), "wb");
  long filelen = img_stream.size();             // Get the current byte offset in the file
  fwrite(img, sizeof *img,filelen , img_in);
  fclose(img_in); // Close the file
	flag = true;
	return flag;
}

vector<string> img_names(){
	string temp;
	temp = "ls ../static/"+my_name()+"_mimgs/ > ../static/"+my_name()+".txt";
	cout<<endl <<temp<<"ANA TEMP"<<endl;
	system(temp.c_str());
	system("sync");
	std::ifstream fin{"../static/"+my_name()+"_mimgs.txt"};
	string img;
	my_images.clear();
	while(fin>>img){
		cout << "IMAGE " << img<< endl;
		my_images.push_back(img);
	}
	cout << "ANA ITNADHT" << endl;
	return my_images;
}

vector<string> get_img_names(string user_name){
	return get<0>(request<vector<string>>(user_name, "get_img_names", tuple<string>{user_name}));
}
