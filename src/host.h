#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string>

#ifndef __HOST__H_
#define __HOST__H_

/*!Holds info about hosts & ports*/
class host{ // TODO: issue with ip assignment
            // TODO: _port assignment in cons == 0
public:
  host(std::string="", int=0); // 0 choose any available port
  int port();
  void print_info();
  struct sockaddr * address(); // shoud be const &
  struct sockaddr_in* address_in();
  int address_size();
  std::string hostname();
private:
  int _port;
  std::string _hostname;
  struct sockaddr_in _address;
};

extern "C" {
  char * inet_ntoa(struct in_addr);
}

#endif //__HOST__H_
