#include "udp_socket.h"

using std::cout;
using std::endl;

/*!Create a kernel socket*/
udp_socket::udp_socket(bool timeout){
  if((s = socket(AF_INET, SOCK_DGRAM, 0))<0) {
    perror("Error: socket create failed");
    return;
  }
  if(timeout){
    struct timeval tv; //TODO: choose apropriate timeout value (1s is too long)
    tv.tv_usec = 0;
    tv.tv_sec = 1;
    if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
      perror("Error: socket timeout failed");
    }
  }

  setBroadcast(s);
}

bool udp_socket::ibind(host h) {
  socket_host = h;
  printf("info in bind: ");
  h.print_info();
  cout << endl;
  if( bind(s, socket_host.address(), socket_host.address_size())!= 0){
    perror("Bind failed\n");
    close (s);
    return false;
  }
  return true;
}

int udp_socket::isendto(message* msg, host h){
  int n;
  // cout << "isendto: " << msg->marshal() << " size:" << msg->size() << " len: " << strlen(msg->marshal())<< endl;
  if( (n = sendto(s, msg->marshal(), msg->size(), 0, h.address(),
    h.address_size())) < 0) perror("Send 2 failed\n");
  //cout << " ISEND ERROR : " << strlen(msg->marshal()) << " " << msg->size() << endl;
  return n;
}

std::pair<host, int> udp_socket::irecvfrom(message* msg) {
  //TODO: return a pair of <host, message*>
  //      Does not take a parameter.
  host h;
  int n;
  int aLength = h.address_size();
  h.address_in()->sin_family = AF_INET;
  char* des = (char*) msg->marshal();
  if((n = recvfrom(s, des, msg->size(), 0, h.address(), (socklen_t*) &aLength))< 0){
    *des = 0;
  }
  else{
    *(des+n) = 0;
  }
  // cout << "irecvfrom: " << msg->marshal()  << " n: " << n << endl;
  return std::make_pair(h, n);
}

udp_socket::~udp_socket(){
  close(s);
}

void setBroadcast(int s)
{
  int arg;
  #ifdef SO_BROADCAST
  arg =1;
  if(setsockopt(s, SOL_SOCKET, SO_BROADCAST, &arg, sizeof(arg)) <0){
    perror("setsockopt SO_BROADCAST---");
    exit(-1);
  }
  #endif
}
