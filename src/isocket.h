#include "host.h"
#include "message.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <utility>

#ifndef __ISOCKET__H_
#define __ISOCKET__H_

/*Abstract socket*/
class isocket {
public:
  virtual bool ibind(host) = 0;
  virtual int isendto(message*, host) = 0;
  virtual std::pair<host, int> irecvfrom(message*) = 0; // TODO: return a message
  virtual ~isocket(){};
};

#endif //__ISOCKET__H_
