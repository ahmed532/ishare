#include "message.h"
#include <string.h>

#ifndef __CHAR_MESSAGE__H_
#define __CHAR_MESSAGE__H_

/*!Implements a char message*/
class char_message : public message {
public:
  const char* marshal();
  int size();
  char_message(char*, char*);
  char_message(int);
  ~char_message();

private:
  char* str;
  int _size;
};

#endif //__CHAR_MESSAGE__H_
