#include "request_reply.h"

tcp ztcp{true};
// tcp ytcp{true};

std::string _dir_name = "directory";
std::string _my_name = "Refaat";
std::string _sender_name = "";
int __rquota_i;
std::unordered_map<std::string, int> name_to_fun_num{
  {"get_img", 0},
  {"get_dir",2},
  {"change_qouta", 1},
  {"login", 3},
  {"sign_up", 7},
  {"get_img_names", 6},
  {"recieve_img",8}
};

std::string dir_name(){
	return _dir_name;
}

std::string my_name(){
	return _my_name;
}

std::string sender_name(){
  return _sender_name;
}

data request_helper(host h, data d, tcp& xtcp) {
  data rec;
  char* m = xtcp.marshal(d);
  xtcp.tcp_send(m, sizeof d + 10 + 10 + d.size, h);
  char* r = xtcp.tcp_receive();
  rec = xtcp.unmarshal(r);
  return rec;
}

data listen(host h, tcp& xtcp){
  return xtcp.unmarshal(xtcp.tcp_receive());
}

bool reply(host h, data d, tcp& xtcp){
  char* m = xtcp.marshal(d);
  xtcp.tcp_send(m, sizeof d + 10 + 10 + d.size, h);
  return xtcp.tcp_packets_size == xtcp.tcp_counter;
}

char* array_byting(std::vector<uint8_t> st)
{
    char* m = new char[max_byte_size];
    for(size_t i = 0; i < st.size(); ++i)
      memcpy(m+i,(char*) &st[i],1);
    return m;
}

StreamType byte_streaming(char* begin,char* end)
{
  std::vector<uint8_t>v(begin,end);
  return v;
}

void f(){
  std::cout << "F: runnning" << std::endl;
}
int g(int x){
  std::cout << "runngin G " << x<< std::endl;
  return 0;
}

int inc_quota(int k)
{
  return k;
}
