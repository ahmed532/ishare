#include "server.h"
#include "isocket.h"
#include "udp_socket.h"
#include "host.h"
#include "char_message.h"
#include <iostream>

#ifndef __ECHO_SERVER__H_
#define __ECHO_SERVER__H_

/*!Implements a server that echos messages back to clients*/
class echo_server : public server {
public:
  echo_server(host);
  void exec();
  void print_info();
  std::pair<message*, host> receive();
  bool reply(message*, host);
  ~echo_server();
private:
  isocket* s;
  int count;
};

#endif //__ECHO_SERVER__H_
