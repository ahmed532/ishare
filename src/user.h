#include <tuple>
#include <string>
#include <unordered_map>
#include <vector>

#ifndef __USER__H_
#define __USER__H_
using std::tuple;
using std::unordered_map;
using std::string;
using std::vector;
using std::get;

typedef tuple<string, int> user_info;

struct hash_2_strings{
	size_t
	operator()(tuple<string, string> const& tt) const {
			std::hash<string> hs;
	    return hs(std::get<0>(tt)) ^ hs(std::get<1>(tt));
	}
};

extern unordered_map<string, user_info> users;
extern unordered_map<tuple<string, string>, int, hash_2_strings> qoutas;
extern vector<string> my_images;

#endif //__USER__H_
