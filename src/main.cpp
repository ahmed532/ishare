#include <bits/stdc++.h>
#include <fstream>
#include <iostream>
#include <string>
#include <stdio.h>
#include <unordered_map>
#include <unistd.h>
#include "serialize.h"
#include <utility>
#include "tcp.h"
#include "request_reply.h"
#include "service.h"
#include "dispatch.h"
#include "udp_socket.h"
#include <thread>
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::cin;

void client_p(){
	// ytcp.udp.ibind(host("0.0.0.0", p));
	read_dir();
	while(true){
		cout << "Enter com: ";
		int x;
		cin >> x;
		if(x == 1){
			string user, img, imgo;
			cout << "Enter user img oimg: ";
			cin >> user >> img >> imgo;
			auto v = get_img(user, img);
			char* p = array_byting(v);
			FILE *fileptr;
			long filelen;

			fileptr = fopen(imgo.c_str(), "wb");  // Open the file in binary mode
			filelen = v.size();
			fwrite(p, sizeof *p, filelen, fileptr);
			fclose(fileptr); // Close the file
		}
		else if(x == 0){
			string user, ip;
			cout << "Enter user ip: ";
			cin >> user >> ip;
			_my_name = user;
			try{
				login(user, ip);
			}
			catch(timeout_error& e){
				cout << e.what() << endl;
				exit(0);
			}
	}else if(x == 8)
			{
				string user, img;
				cout << "Enter user img: ";
				cin >> user >> img;
				bool flag = send_to_user(user,img);
			}
	}
}

void service_p(int p){
	ztcp.udp.ibind(host("0.0.0.0", p));
	read_dir();
	while(true){
		data ud = listen(host("0.0.0.0", p), ztcp);
		cout << ud.owner << " " << ud.receiver << endl;
		dispatch(ud);
		cout <<"saved sender: "<< sender_name() << endl;
		cout << "======================" << endl;
	}
}

void direc(int p){
	ztcp.udp.ibind(host("0.0.0.0", p));
  read_dir();
  while(true){
    data ud = listen(host("0.0.0.0", p), ztcp);
    cout << ud.owner << " " << ud.receiver << endl;
	  dispatch(ud);
		cout << "=================="<<endl;
  }
}

int main(int argc, char** argv) {
	if(argc == 1){
		direc(1030);
	}
	else{
		int p1;
		p1 = std::stoi(argv[1]);
		std::thread c(client_p);
		std::thread s(service_p, p1);
		s.detach();
		c.join();
	}
  return 0;
}
