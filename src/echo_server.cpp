#include "echo_server.h"

using std::cout;
using std::cin;
using std::endl;

/*!Creates and binds to a socket*/
echo_server::echo_server(host h){
  s = new udp_socket(false);
  count = 0;
  if(!s->ibind(h)){
    cout << "socket ibind error" << endl;
  }
}
//TODO: Implement it
void echo_server::print_info(){
  ;
}

/*!Wait for a message and echo it back*/
void echo_server::exec(){
  auto p{receive()};
  //cout << p.first->marshal() << endl; // Leeh te print el pointer
  while(true){
    reply(p.first, p.second);
    p = receive();
  }
}
//TODO: move this to net_node
std::pair<message*, host> echo_server::receive(){
  message* rec = new char_message(100); //TODO: delete rec, maybe make it member
                                        //TODO: 100 is arbitrary
  std::pair<host, int> p = s->irecvfrom(rec);
  // *(rec->marshal()+p.second) = 0;
  p.first.print_info();
  cout << "$ " <<rec->marshal() << "\t";
  cout << "MSG RECIVE COUNT: " << ++count << endl;
  return std::make_pair(rec, p.first);
}

bool echo_server::reply(message* msg, host h){ //TODO: Send the pair instead of
                                               //      msg & host
  int n = s->isendto(msg, h) != 0;
  return n;
}

echo_server::~echo_server(){
  delete s;
}
