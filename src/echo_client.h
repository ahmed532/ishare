#include "client.h"
#include "message.h"
#include "isocket.h"
#include "char_message.h"
#include "udp_socket.h"
#include "host.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <istream>

#ifndef __ECHO_CLIENT__H_
#define __ECHO_CLIENT__H_

/*!Implements a client that echos some stream to a server*/
class echo_client : public client{
public:
  echo_client();
  void exec(host h, std::istream&);
  message* send(message*, host);
  ~echo_client();
private:
  isocket* s;
  int input_size;
};

#endif //__ECHO_CLIENT__H_
