#include "char_message.h"

/*!Copy from buffer*/
char_message::char_message(char* begin, char* end) {
  _size = end - begin;
  str = new char[_size+1];
  int i = 0;
  while(i < _size){
    *str++ = *begin++;
    i++;
  }
  *str = 0;
  str -= _size;
}

/*!Create an empty message of size s*/
char_message::char_message(int s)
:_size(s){
  str = new char[_size+1];
  memset(str, 0, _size+1);
}

const char* char_message::marshal(){ //TODO: Should return a const pointer
                               //      Or a copy
    // char* des=new char [100];
    // strcpy(des,str);
    return (const char*) str;
}

int char_message::size(){
  return _size;
}

char_message::~char_message(){
  delete[] str;
}
