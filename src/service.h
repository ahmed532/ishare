#include <string>
#include <unordered_map>
#include <fstream>
#include <vector>
#include <iostream>
#include <functional>
#include <fstream>
#include <stdio.h>
#include <bits/stdc++.h>
#include "request_reply.h"
#include "user.h"


#ifndef __SERVICE__H_
#define __SERVICE__H_
//TODO: check if file exists before opening (causes shit load of runtime errors)
using std::string;
using std::vector;
using std::unordered_map;
using std::tuple;
using std::endl;
using std::ofstream;
using std::get;
using std::cout;

bool register_user(string user_name, string ip);

bool verify_user(string user_name, string ip);

bool login(string user_name, string ip);

bool sign_up(string user_name, string ip);

vector<tuple<string, user_info>> dir();

vector<tuple<string, user_info>> get_dir();

vector<uint8_t> get_img(string user_name, string img_name);

vector<uint8_t> send_img(string img_name);

bool send_to_user(string user_name,string img_name);

bool recieve_img(vector<uint8_t> img_stream,string img_name);

vector<string> img_names();

vector<string> get_img_names(string user_name);

void read_dir();

#endif //__SERVICE__H_
