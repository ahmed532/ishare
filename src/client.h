#include "message.h"
#include "isocket.h"
#include "host.h"
#include <istream>

#ifndef __CLIENT__H__
#define __CLIENT__H__

//TODO: inheret from net_node
/*!Abstract client*/
class client{
public:
  virtual void exec(host h, std::istream&) = 0;
  virtual message* send(message*, host) = 0;
  virtual ~client(){};
};

#endif // __CLIENT__H__
