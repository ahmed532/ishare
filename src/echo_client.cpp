#include "echo_client.h"

using std::cout;
using std::cin;
using std::endl;

/*!Creates and binds to a socket*/
echo_client::echo_client(){//TODO:Maybe client wants to define himself
  input_size = 100;
  s = new udp_socket{false};
  if(!s->ibind(host(""))){
    cout << "socket ibind error" << endl;
  }
}

/*!read a stream and send it line by line to a server*/
void echo_client::exec(host h, std::istream& is){
  std::string input="bombard";
  int count = 0;
  cout << "> ";
  while(!is||(std::getline(is,input) && input != "q")){
    //  cout << "client: " << input << " " << n << endl;
    char ii[input_size];
    strcpy(ii, input.c_str());
    message* msg = new char_message(ii, ii+input_size);
    // cout << "client copied: " << msg->marshal() << " " << msg->size() << endl;
    message* rep = send(msg, h);
    //cout << "client marshal: " <<rep->marshal() << endl;
    cout<< rep->marshal() << "\t";
    cout << "MSG COUNT: " << ++count << endl;
    cout << "> ";
  }
}

//TODO: move this to net_node
/*!Send a message and wait for reply*/
message* echo_client::send(message* msg, host h){
  int n = s->isendto(msg, h);
  if(n < msg->size()); // TODO: Print error
  message* rep = new char_message(100);
  auto p = s->irecvfrom(rep);
  return rep;
}

echo_client::~echo_client(){
  delete s;
}
