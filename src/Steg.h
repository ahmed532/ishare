#pragma once
#ifndef STEG_fH
#define STEG_H

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <sstream>
using namespace std;

class Steg {
private:

public:

Steg(char, string);
bool steg_dr();  //Stegs and Deletes Files
bool steg_ds();  //Stegs and Deletes Files
int unsteg_c();
int get_quota();
void sen_paths(string);
void rec_pathsr(string);
bool check_allwd();

void unsteg_v(bool,bool,bool,int);
const char* revoke_acc(const char*);
const char* change_quota(const char*,int);
const char* incdec_quota(const char*,bool);

void write_f(const char*, int, string);
const char* read_f(string, int& size);

int quota_c,quota_v;
string owner,user,quota;
bool revoke=0,inc_dec=0,change=0;
int image_size, mask_size, data_size;
const string config_path="config.txt";
string st_comm1,st_comm2,unst_comm1,unst_comm2,rm_comm1,rm_comm2;
string imgname;
string cpycomm;
string mask_path,image_path,data_path;
string pass = "amrelkadi";

};
#endif
