
#ifndef __MESSAGE__H_
#define __MESSAGE__H_

/*!Abstract message*/
class message{
public:
  virtual const char *marshal() = 0;
  virtual int size() = 0;
  virtual ~message(){};
};

#endif //__MESSAGE__H_
