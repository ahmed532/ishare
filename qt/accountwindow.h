#pragma once
#ifndef ACCOUNTWINDOW_H
#define ACCOUNTWINDOW_H

#include <QWidget>
#include <QString>
#include <QPixmap>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include "sendwindow.h"
#include "requestwindow.h"
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
using namespace std;

namespace Ui {
class AccountWindow;
}

class AccountWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AccountWindow(QWidget *parent = 0);
    ~AccountWindow();

    void getusers(string);
    void getusers();
    // void getlocalimages(string);
    void getlocalimages();
    void getreceivedimages();

private slots:
    void on_Send_clicked();
    void on_Request_clicked();
    void on_mimages_itemDoubleClicked(QListWidgetItem *item);
    void on_rimages_itemDoubleClicked(QListWidgetItem *item);
//    void on_usrs_itemClicked(QListWidgetItem *item);


    void on_acc_clicked();

    void on_rej_clicked();

private:
    Ui::AccountWindow *ui;
    sendwindow *send_pg;
    requestwindow *req_pg;
    string limg_path,rimg_path,usrs_path;
    string v_img;
    QPixmap view;
};
#endif // ACCOUNTWINDOW_H
