#pragma once
#ifndef WELCOME_H
#define WELCOME_H

#include <QWidget>
#include <QTimer>
#include <QMainWindow>
#include <QMessageBox> //For Errors information/warning (this,title,texttoshow)
#include"loginwindow.h"
#include <iostream>
#include <string.h>
#include <string>
using namespace std;

namespace Ui {
class welcome;
}

class welcome : public QWidget
{
    Q_OBJECT

public:
    explicit welcome(QWidget *parent = 0);
    ~welcome();

private slots:
    void on_start_clicked();

private:
    Ui::welcome *ui;
    LoginWindow *log;
};

#endif // WELCOME_H
