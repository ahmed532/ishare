#pragma once
#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include <QMessageBox> //For Errors information/warning (this,title,texttoshow)
#include"accountwindow.h"
#include <iostream>
#include <string.h>
#include <string>
using namespace std;

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LoginWindow(QWidget *parent = 0);
    ~LoginWindow();
    int f();
    string newuser, username, ip_addr;

private slots:
    void on_Register_clicked();
    void on_Login_clicked();

private:
    Ui::LoginWindow *ui;
    AccountWindow *acc_pg;
    QString LOG_N, REG_N, IP_ADDR;
};

#endif // LOGINWINDOW_H
