#include "welcome.h"
#include "ui_welcome.h"

welcome::welcome(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::welcome)
{
    ui->setupUi(this);
    string log="../static/logo.png";
    QPixmap view(log.c_str());
    int w = ui->logo->width();
    int h = ui->logo->height();
    ui->logo->setPixmap(view.scaled(w,h,Qt::KeepAspectRatio));
    //ui->logo->setPixmap(view);
    ui->logo->setEnabled(true);
}
welcome::~welcome()
{
    delete ui;
}

void welcome::on_start_clicked()
{
    hide();
    log = new LoginWindow();
    log->show();
}
