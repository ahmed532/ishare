#pragma once
#ifndef REQUESTWINDOW_H
#define REQUESTWINDOW_H

#include <QWidget>
#include <QString>
#include <QFileDialog>
#include <QListWidgetItem>
#include <iostream>
#include <fstream>
#include <string.h>
#include <string>
using namespace std;

namespace Ui {
class requestwindow;
}
class requestwindow : public QWidget
{
    Q_OBJECT

public:
    explicit requestwindow(QWidget *parent = 0);
    ~requestwindow();

    void getusers();
    void getreceivedimages();
    void getuserimages(string);

private slots:

    void on_Request_clicked();
    void on_recimgs_itemClicked(QListWidgetItem*item);
    void on_recimgs_r_itemClicked(QListWidgetItem *item);
    void on_usrs_r_itemClicked(QListWidgetItem *item);
    void on_Request_Quota_clicked();



private:
    Ui::requestwindow *ui;
    string rf_usr, rf_img;
    string ch_quota,rc_img;
};

#endif // REQUESTWINDOW_H
