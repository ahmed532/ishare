#include "accountwindow.h"
#include "ui_accountwindow.h"
#include "../src/service.h"
#include "../src/Steg.h"

using namespace std;

AccountWindow::AccountWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AccountWindow)
{
    ui->setupUi(this);
}
AccountWindow::~AccountWindow()
{
    delete ui;
}
// void AccountWindow::getlocalimages(string user)
void AccountWindow::getlocalimages()
{// EL MFROOD YGEEB EL SOWAR BTA3t EL USER EL HWA ANA BS???
    string dircomm ="mkdir ../static/"+my_name()+"_mimgs";
    string myimgcomm ="ls ../static/"+my_name()+"_mimgs > ../static/"+my_name()+"_mimgs.txt";
    system(dircomm.c_str());
    system(myimgcomm.c_str());
    ifstream rd_ulimg;
    QString temp_s;
    string s;
    string temp="../static/" + my_name() +"_mimgs.txt";
    rd_ulimg.open(temp);
    ui->mimages->clear();
    while(!rd_ulimg.eof())
    {
        getline(rd_ulimg,s);
        // cout << s << " ana esm el sora" <<endl;
        if(s.length()>0)
          s=s.substr(1,s.length());
        temp_s =  QString::fromStdString(s);
        ui->mimages->addItem(temp_s);
    }
}
void AccountWindow::getreceivedimages()
{
    string dircomm ="mkdir ../static/"+my_name()+"_rimgs";
    string rimgcomm ="ls ../static/"+my_name()+"_rimgs > ../static/"+my_name()+"_rimgs.txt";
    system(dircomm.c_str());
    system(rimgcomm.c_str());
    ifstream rd_rimg;
    QString temp_s;
    string s;
    string temp ="../static/" + my_name() +"_rimgs.txt";
    //USE rimg_path
    rd_rimg.open(temp);
    ui->rimages->clear();
    while(!rd_rimg.eof())
    {
      getline(rd_rimg,s);
      // if(s.length()>1)
      // s=s.substr(1,s.length());
      temp_s=QString::fromStdString(s);
      ui->rimages->addItem(temp_s);
    }
}
void AccountWindow::getusers()
{
   QString temp_s;
   string s;
  //  ui->usrs->clear();
   auto v = get_dir();
   for(auto& u : v)
   {
     s = get<0>(u);
     cout << "*" << s << "* " <<get<0>(get<1>(u))<< endl;
     temp_s=QString::fromStdString(s);
    //  ui->usrs->addItem(temp_s);
   }
}
void AccountWindow::getusers(string nusr)
{
 getusers();
 // QString usrn=QString::fromStdString(nusr);
 // ui->usrs->addItem(usrn);
}
void AccountWindow::on_Send_clicked()
{
    hide();
    send_pg=new sendwindow();
    send_pg->show();
    send_pg->getusers();
    send_pg->getlocalimages();
}
void AccountWindow::on_Request_clicked()
{
    hide();
    req_pg=new requestwindow();
    req_pg->show();
    req_pg->getusers();
    //req_pg->getuserimages();
    req_pg->getreceivedimages();
}
void AccountWindow::on_mimages_itemDoubleClicked(QListWidgetItem *item)
{
    QString vimg_path=item->text();
    v_img="../static/"+my_name()+"_mimgs/x" + vimg_path.toStdString();
    int w = ui->imgv->width();
    int h = ui->imgv->height();
    QPixmap view(v_img.c_str());
    // ui->imgv->setPixmap(view);
    ui->imgv->setPixmap(view.scaled(w,h,Qt::KeepAspectRatio));
    ui->imgv->setEnabled(true);
}
//void AccountWindow::on_usrs_itemClicked(QListWidgetItem *item)
//{
//    QString qimg_path=item->text();
//    string rf_usr= qimg_path.toStdString();
//    getlocalimages(rf_usr);
//    getreceivedimages();
//    cout << rf_usr <<endl;
//}
void AccountWindow::on_rimages_itemDoubleClicked(QListWidgetItem *item)
{
    QString vimg_path=item->text();
    v_img="../static/"+my_name()+"_rimgs/" + vimg_path.toStdString();
    Steg rec('r',vimg_path.toStdString());

    int w = ui->imgv->width();
    int h = ui->imgv->height();
    string temp ="../static/"+my_name()+"_rimgs/x"+vimg_path.toStdString();
    if(rec.check_allwd())
    {

      string temp ="../static/"+my_name()+"_rimgs/x"+vimg_path.toStdString();
      rec.unsteg_v(0,0,0,0);
      QPixmap view(temp.c_str());
      ui->imgv->setPixmap(view.scaled(w,h,Qt::KeepAspectRatio));
      ui->imgv->setEnabled(true);
      rec.steg_dr();
    }
    else
    {
      string temp ="../static/"+my_name()+"_rimgs/"+vimg_path.toStdString();
      // cout <<temp.c_str() << " ANA EL WA&SH\n";
      QPixmap view(temp.c_str());
      ui->imgv->setPixmap(view.scaled(w,h,Qt::KeepAspectRatio));
      ui->imgv->setEnabled(true);
      rec.steg_dr();
    }
    // QPixmap view(v_img.c_str());
    // ui->imgv->setPixmap(view);
    // ui->imgv->setPixmap(view.scaled(w,h,Qt::KeepAspectRatio));
    // ui->imgv->setEnabled(true);
    // rec.steg_dr();
}

// CREATE A FUNCTION
// IF receieved an image ui->recntf->setEnabled(true);
void AccountWindow::on_acc_clicked()
{
//    ui->recntf->setVisible(false);
   ui->recntf->setEnabled(false);
    // Accept the received image
    this->repaint();
}

void AccountWindow::on_rej_clicked()
{
      ui->recntf->setEnabled(false);
//   ui->recntf->setVisible(false);
   // Reject the received image
   this->repaint();

}
