#include "requestwindow.h"
#include "ui_requestwindow.h"
#include "../src/service.h"
#include "../src/request_reply.h"

using namespace std;
requestwindow::requestwindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::requestwindow)
{
    ui->setupUi(this);
}
requestwindow::~requestwindow()
{
    delete ui;
}
void requestwindow::getusers()
{
  QString temp_s;
  string s;
  ui->usrs_r->clear();
  auto v = get_dir();
  for(auto& u : v)
  {
    s = get<0>(u);
    cout << "*" << s << "* " <<get<0>(get<1>(u))<< endl;
    temp_s=QString::fromStdString(s);
    ui->usrs_r->addItem(temp_s);
  }
  //
  // ifstream rd_usrs;
  // QString temp_s;
  // string s;
  // //User Directory
  // rd_usrs.open("../static/users.txt");
  // while(!rd_usrs.eof())
  // {
  //   getline(rd_usrs,s);
  //   temp_s =  QString::fromStdString(s);
  //   ui->usrs_r->addItem(temp_s);
  // }
}
void requestwindow::getreceivedimages()
{
  string dircomm ="mkdir ../static/"+my_name()+"_rimgs";
  string rimgcomm ="ls ../static/"+my_name()+"_rimgs > ../static/"+my_name()+"_rimgs.txt";
  system(dircomm.c_str());
  system(rimgcomm.c_str());
  ifstream rd_rimg;
  QString temp_s;
  string s;
  string temp ="../static/" + my_name() +"_rimgs.txt";
  //USE rimg_path
  rd_rimg.open(temp);
  // ifstream rd_rimg;
  // QString temp_s;
  // string s;
  // rd_rimg.open("../static/rimages.txt");
  ui->recimgs_r->clear();
  while(!rd_rimg.eof())
  {
    getline(rd_rimg,s);
    temp_s =  QString::fromStdString(s);
    ui->recimgs_r->addItem(temp_s);
  }
}
//TODO:GET RECEIVED IMAGES FILE
void requestwindow::getuserimages(string user)
{ //get local images of each user when selected by name
  ifstream rd_ulimg;
  QString temp_s;
  //string temp="../static/" + user +".txt";
  //rd_ulimg.open(temp);
  auto v = get_img_names(user);
  ui->recimgs->clear();
  for(auto& s : v)
  {
    if(s.length()>0)
    s= s.substr(1,s.length());
    temp_s =  QString::fromStdString(s);
    ui->recimgs->addItem(temp_s);
  }
}
void requestwindow::on_Request_clicked()
{
    //Invokes a Request with the image path
    QString rquota=ui->requota->text();
    // QString rquota;
    string rsquota=rquota.toStdString();
    __rquota_i = stoi(rsquota);
    cout << rsquota << "ANA RQUOTA" << endl;
    string temp;
    temp ='x'+rf_img;
    cout <<"I AM SENDING " << temp <<endl;
    // auto v = get_img(rf_usr, rf_img);
    auto v = get_img(rf_usr, temp);

    char* p = array_byting(v);
    FILE *fileptr;
    long filelen;
    cout << rf_img <<endl;
    string savep= "../static/"+my_name()+"_rimgs/"+rf_img;
    cout <<"I AM WRITING"<<endl;
    // fileptr = fopen(rf_img.c_str(), "wb");  // Open the file in binary mode
    fileptr = fopen(savep.c_str(), "wb");  // Open the file in binary mode
    filelen = v.size();
    fwrite(p, sizeof *p, filelen, fileptr);
    fclose(fileptr); // Close the file
}

void requestwindow::on_recimgs_itemClicked(QListWidgetItem *item)
{//Images to Request From
    QString qimg_path=item->text();
    rf_img=qimg_path.toStdString();
    cout << rf_img <<endl;
}
void requestwindow::on_recimgs_r_itemClicked(QListWidgetItem *item)
{
  //Images to Request From
    QString qimg_path=item->text();
    rc_img=qimg_path.toStdString();
    cout << rc_img <<endl;
}
void requestwindow::on_usrs_r_itemClicked(QListWidgetItem *item)
{//User to Request From
    QString qimg_path=item->text();
    rf_usr= qimg_path.toStdString();
    getuserimages(rf_usr);
    cout << rf_usr <<endl;
}
void requestwindow::on_Request_Quota_clicked()
{
  QString rquota=ui->requota->text();
  // QString rquota;
  ch_quota=rquota.toStdString();
  cout << ch_quota << endl;
  string temp = "../static/"+rf_usr+"_"+rc_img+"_q"+".txt";
  ofstream test(temp);
  test <<rf_usr;
  test <<endl;
  test <<my_name();
  test <<endl;
  test <<ch_quota;
  test.close();
  //Hatcall hna an rpc for the unsteg_v function
  //with the parameters(0,1,0,rf_quota)
  //
  //and send the req.txt with it
}
