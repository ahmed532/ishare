#include "loginwindow.h"
#include "accountwindow.h"
#include "welcome.h"
#include <QApplication>
#include<iostream>
#include <thread>
#include "../src/request_reply.h"
#include "../src/dispatch.h"

using namespace std;

void run_client_gui(int argc, char *argv[]){
  QApplication a(argc, argv);
  welcome w1;
  w1.show();
  a.exec();
}

void run_service(int p) {
  ztcp.udp.ibind(host("0.0.0.0", p));
	read_dir();
	while(true){
		data ud = listen(host("0.0.0.0", p), ztcp);
		cout << ud.owner << " " << ud.receiver <<" "<< ud.quota << " " << ud.size << endl;
		cout << ud.img << endl;
		dispatch(ud);
	}
}

void direc(int p){
	ztcp.udp.ibind(host("0.0.0.0", p));
  read_dir();
  while(true){
    data ud = listen(host("0.0.0.0", p), ztcp);
    cout << ud.owner << " " << ud.receiver <<" "<< ud.quota << " " << ud.size << endl;
    cout << ud.img << endl;
	  dispatch(ud);
  }
}

int main(int argc, char *argv[])
{
  if(argc == 1){
		direc(1030);
	}
	else{
    int p;
    p = std::stoi(argv[1]);
    std::thread s(run_service, p);
    std::thread c(run_client_gui, argc, argv);
    s.detach();
    c.join();
	}
  return 0;
}
