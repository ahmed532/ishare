#-------------------------------------------------
#
# Project created by QtCreator 2017-12-03T22:35:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QT_1
TEMPLATE = app


SOURCES += main.cpp\
    accountwindow.cpp \
    loginwindow.cpp \
    requestwindow.cpp \
    sendwindow.cpp \
    welcome.cpp \
    ../src/client.cpp ../src/server.cpp ../src/message.cpp ../src/char_message.cpp \
    ../src/isocket.cpp ../src/host.cpp ../src/udp_socket.cpp ../src/echo_client.cpp ../src/echo_server.cpp \
    ../src/tcp.cpp ../src/Steg.cpp ../src/user.cpp ../src/service.cpp ../src/request_reply.cpp

HEADERS  += welcome.h \
    accountwindow.h \
    requestwindow.h \
    loginwindow.h \
    sendwindow.h \
    ../src/client.h ../src/server.h ../src/message.h ../src/char_message.h \
    ../src/isocket.h ../src/host.h ../src/udp_socket.h ../src/echo_client.h ../src/echo_server.h \
    ../src/tcp.h ../src/Steg.h ../src/user.h ../src/service.h ../src/request_reply.h

FORMS    += accountwindow.ui \
    sendwindow.ui \
    requestwindow.ui \
    loginwindow.ui \
    welcome.ui

DISTFILES +=
