#pragma once
#ifndef SENDWINDOW_H
#define SENDWINDOW_H

#include <QWidget>
#include <QString>
#include <QFileDialog>
#include <QListWidgetItem>
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include "../src/request_reply.h"
using namespace std;

namespace Ui {
class sendwindow;
}
class sendwindow : public QWidget
{
    Q_OBJECT

public:
    explicit sendwindow(QWidget *parent = 0);
    ~sendwindow();

    void getusers();
    void getlocalimages();

private slots:
    void on_AddImage_clicked();
    void on_Send_clicked();
    void on_mimages_s_itemClicked(QListWidgetItem *item);
    void on_usrs_s_itemClicked(QListWidgetItem *item);

private:
    Ui::sendwindow *ui;
    string limg_path;
    string s_path,sto_usr,s_quota;
};
#endif // SENDWINDOW_H
